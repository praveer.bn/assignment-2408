package day0824.Assignmanet;

import java.util.Random;
import java.util.UUID;

class Bank{
    public String name;
    public String email;
    private int id;
    private int password;
    private String account_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    @Override
    public String toString() {
        return "ICICI Bank{" +
                "name of Account holder='" + name + '\'' +
                ", email of Account holder='" + email + '\'' +
                ", id=" + id +
                ", password=" + password +
                ", account_number=" + account_number +
                '}';
    }
}

public class BankSystem {
    public static void main(String[] args) {
        Bank account_holder1=new Bank();
        Random ra=new Random();
        int ra_int=ra.nextInt(1000);


        String a=UUID.randomUUID().toString().replace("-", "");
        account_holder1.name = "Praveer";
        account_holder1.email ="praveer@gamil.com";
        account_holder1.setAccount_number(a);
        account_holder1.setId(1234);
        account_holder1.setPassword(ra_int);

        System.out.println(a);
        System.out.println(account_holder1);

    }
}
